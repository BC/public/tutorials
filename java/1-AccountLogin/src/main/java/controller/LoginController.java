/*
 * Copyright (c) 2010-2024 Belledonne Communications SARL.
 *
 * This file is part of Linphone Java Tutorial.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import service.CoreService;

import org.linphone.core.Account;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.core.RegistrationState;
import org.linphone.core.TransportType;

public class LoginController extends CoreListenerStub implements Initializable {
    @FXML
    private Button loginButton;
    @FXML
    private Button logoutButton;
    @FXML
    private TextField identityField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private RadioButton tlsRadio;
    @FXML
    private RadioButton tcpRadio;
    @FXML
    private RadioButton udpRadio;
    @FXML
    private Label loginLabel;
    @FXML
    private Label registrationLabel;

    private CoreService coreService = CoreService.instance();

    @FXML
    public void initialize(URL location, ResourceBundle resourceBundle) {
        // The Core is the main object of the SDK. You can't do much without it.
        // If you're not familiar with Linphone Core creation, see the 0-HelloWorld project.
        Core core = CoreService.instance().core;
    
        // In this tutorial we are going to log in and our registration state will change.
        // To get callbacks from the Core and be notified of the registration state change,
        // we need to register ourself as a listener of the core.
        core.addListener(this);
    
        // Start the core after setup, and before everything else.
        coreService.start();

        // Setup GUI.
        logoutGuiChanges();

        // Now use the GUI to log in, and see LogInClick to see how to handle login.
    }

    // Called when you click on the "Login" button.
    @FXML
    private void onLoginClicked() {
        if (loginButton.isDisabled())
            return;
        loginButton.setDisable(true);
        TransportType transport = TransportType.Udp;
        if (tlsRadio.isSelected()) {
            transport = TransportType.Tls;
        } else if (tcpRadio.isSelected()) {
            transport = TransportType.Tcp;
        }
        coreService.login(identityField.getText(), passwordField.getText(), transport);
    }

    // Called when you click on the "Logout" button.
    @FXML
    private void onLogoutClicked() {
        if (logoutButton.isDisabled())
            return;
        logoutButton.setDisable(true);
        coreService.logout();
    }

    private void logoutGuiChanges() {
        loginButton.setDisable(false);
        logoutButton.setDisable(true);
        loginLabel.setText("You are logged out");
    }

    private void loginGuiChanges() {
        loginButton.setDisable(true);
        logoutButton.setDisable(false);
        loginLabel.setText("You are logged in, with identity " + CoreService.instance().core.getIdentity() + ".");
    }

    private void loginInProgressGuiChanges() {
        loginButton.setDisable(true);
        logoutButton.setDisable(true);
        loginLabel.setText("Login in progress, with identity " + CoreService.instance().core.getIdentity() + ".");
    }

    private void loginFailedGuiChanges() {
        loginButton.setDisable(false);
        logoutButton.setDisable(true);
        loginLabel.setText("Login failed, try again.");
    }

    // This method is called every time the RegistrationState is updated by background core's actions.
    // In this example we use this to update the GUI.
    @Override
    public void onAccountRegistrationStateChanged(Core core, Account account, RegistrationState state, String message) {
        registrationLabel.setText("Your registration state is: " + state.toString());
        switch (state) {
            case Cleared:
            case None:
                coreService.clearCoreAfterLogout();
                logoutGuiChanges();
                break;
            case Ok:
                loginGuiChanges();
                break;
            case Progress:
                loginInProgressGuiChanges();
                break;
            case Failed:
                loginFailedGuiChanges();
                break;
            default:
                break;
        }
    }
}
