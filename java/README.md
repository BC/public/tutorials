Java tutorials
====================

Tutorials are numbered 0 to 3, and we recommend you to read them in that order as features from previous tutorials are used in the next ones, such as account login.

Each tutorial is a full project, so you should build it and run it independently.
For each tutorial, you need to extract the Linphone Java SDK zip file in the tutorial directory and run `./gradlew run` to execute it.

Code is being kept as short and simple as possible, and comments explain how and why things are being done.

Full Java API is available [here](http://linphone.org/snapshots/docs/liblinphone/latest/java).