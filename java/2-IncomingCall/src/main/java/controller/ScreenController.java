/*
 * Copyright (c) 2010-2024 Belledonne Communications SARL.
 *
 * This file is part of Linphone Java Tutorial.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package controller;

import java.io.IOException;
import java.util.HashMap;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

class Screen {
    public Parent root;
    public FXMLLoader loader;

    Screen(Parent root, FXMLLoader loader) {
        this.root = root;
        this.loader = loader;
    }
}

public class ScreenController {
    private HashMap<String, Screen> screenMap = new HashMap<>();
    private Scene main;
    private Screen currentScreen;

    public ScreenController(Scene main) {
        this.main = main;
    }

    public void add(String name, String viewFile) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(viewFile));
        Parent root = loader.load();
        screenMap.put(name, new Screen(root, loader));
    }

    public void remove(String name) {
        screenMap.remove(name);
    }

    public void activate(String name) {
        if (currentScreen != null) {
            currentScreen.loader.<ScreenInterface>getController().onNavigatedFrom();
        }
        currentScreen = screenMap.get(name);
        main.setRoot(currentScreen.root);
        currentScreen.loader.<ScreenInterface>getController().onNavigatedTo();
    }
}
