/*
 * Copyright (c) 2010-2024 Belledonne Communications SARL.
 *
 * This file is part of Linphone Java Tutorial.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import controller.ScreenController;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import service.CoreService;
import javafx.fxml.FXMLLoader;

public class LinphoneJavaFX extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/view/LoginPage.fxml"));
        stage.setTitle("Linphone SDK example");
        stage.setScene(new Scene(root));

        ScreenController screenController = new ScreenController(stage.getScene());
        screenController.add("login", "/view/LoginPage.fxml");
        screenController.add("call", "/view/CallPage.fxml");
        screenController.activate("login");
        CoreService.instance().screenController = screenController;
        stage.show();
    }

    @Override
    public void stop() {
        CoreService.instance().stop();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch();
    }

}