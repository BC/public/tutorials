/*
 * Copyright (c) 2010-2024 Belledonne Communications SARL.
 *
 * This file is part of Linphone Java Tutorial.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import org.linphone.core.Core;
import org.linphone.core.Factory;
import org.linphone.core.LogLevel;
import org.linphone.core.LoggingService;

public class LinphoneJavaFX extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        // Core is the main object of the SDK. You can't do much without it

        // To create a Core, we need the instance of the Factory.
        Factory factory = Factory.instance();
        factory.setDataDir(".");

        // Some configuration can be done before the Core is created, for example enable logs.
        LoggingService loggingService = factory.getLoggingService();
        loggingService.setLogLevel(LogLevel.Message);

        // Your Core can use up to 2 configuration files, but that isn't mandatory.
        // The third parameter is the application context, which is *not* mandatory when working with Java.
        // You can now create your Core object.
        Core core = factory.createCore("", "", null);

        // Once you have your core you can start to do a lot of things, get its version for example.
        String version = core.getVersion();

        Label l = new Label("Hello world, Linphone core version is " + version);
        Scene scene = new Scene(new StackPane(l), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}