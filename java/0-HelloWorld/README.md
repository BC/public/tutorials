Hello World tutorial
====================

The purpose of this tutorial is to explain how to add our SDK as a dependency of an Java project and how to create the `Core` object that all our APIs depends on.

Start by taking a look at the `build.gradle` file to see how we reference the Linphone SDK and add it as a dependency of our project.

The user interface will only display the `Core`'s version, but in the next tutorial you will learn how to use it to login your SIP account.